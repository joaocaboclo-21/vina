#include "lib_vina.h"

#define INICIO_ESCRITA 12 //int + size_t
#define BUFFER 1024

void inicia_archiver(char *nomeArchiver){
    FILE *arc = fopen(nomeArchiver, "w");
    int quant_arq = 0;
    size_t local_dir = INICIO_ESCRITA;
    fwrite(&quant_arq, sizeof(int), 1, arc);
    fwrite(&local_dir, sizeof(size_t), 1, arc);
    fclose(arc);
}

void libera_diretorio(struct arquivo **dir, int quant_arq){
    if (!dir)
        return;

    for (int i = 0; i < quant_arq; i++){
        free(dir[i]->caminho);
        free(dir[i]);
    }
    free(dir);
    return;
}

void escreve_diretorio(FILE *archive, struct arquivo **dir, int quant_arq){
    for (int i = 0; i < quant_arq; i++){

        fwrite(&dir[i]->tamanho_caminho, sizeof(dir[i]->tamanho_caminho), 1,  archive);
        fwrite(dir[i]->caminho, dir[i]->tamanho_caminho, 1, archive);
        fwrite(&dir[i]->pos, sizeof(dir[i]->pos), 1, archive);
        fwrite(&dir[i]->local, sizeof(dir[i]->local), 1, archive);
        fwrite(&dir[i]->userID, sizeof(dir[i]->userID), 1, archive);
        fwrite(&dir[i]->permissoes, sizeof(dir[i]->permissoes), 1, archive);
        fwrite(&dir[i]->tamanho, sizeof(dir[i]->tamanho), 1, archive);
        fwrite(&dir[i]->data, sizeof(dir[i]->data), 1, archive);

    }
}

struct arquivo *obtem_info(char *nomeArq){
    struct arquivo *arq = malloc(sizeof(struct arquivo));

    struct stat st;
    stat(nomeArq, &st);

    arq->tamanho_caminho = strlen(nomeArq) + 1;
    arq->caminho = strdup(nomeArq);
    arq->pos = 0;
    arq->userID = st.st_uid;
    arq->permissoes = st.st_mode;
    arq->tamanho = st.st_size;
    arq->data = st.st_mtime;

    return arq;
}

void transporta(FILE* arq, FILE* archive, size_t tamanho){
    int quant_blocos = tamanho / BUFFER;
    size_t resto = tamanho % BUFFER;

    void *buff = malloc(1024);
    for (int i = 0; i < quant_blocos; i++){
        fread(buff, 1024, 1, arq);
        fwrite(buff, 1024, 1, archive);
    }

    buff = realloc(buff, resto);
    fread(buff, resto, 1, arq);
    fwrite(buff, resto, 1, archive);
    free(buff);
}

void copia_parte(FILE* archive, size_t tamanho, size_t local_arq, size_t local_prox){
    int quant_blocos = tamanho / BUFFER;
    size_t resto = tamanho % BUFFER;

    void *buff = malloc(1024);
    for (int i = 0; i < quant_blocos; i++){
        fseek(archive, local_prox, SEEK_SET);
        fread(buff, 1024, 1, archive);

        fseek(archive, local_arq, SEEK_SET);
        fwrite(buff, 1024, 1, archive);

        local_arq += 1024;
        local_prox += 1024;
    }

    buff = realloc(buff, resto);

    fseek(archive, local_prox + (1024)*quant_blocos, SEEK_SET);
    fread(buff, resto, 1, archive);

    fseek(archive, local_arq + (1024)*quant_blocos, SEEK_SET);
    fwrite(buff, resto, 1, archive);

    free(buff);
}

struct arquivo **obtem_diretorio(char *nomeArchive, size_t *local_dir, int *quant_arq){
    FILE *arc = fopen(nomeArchive, "r");

    if (!arc)
        return NULL;

    fread(quant_arq, sizeof(int), 1, arc);
    fread(local_dir, sizeof(size_t), 1, arc);

    if ((*quant_arq) == 0){
        fclose(arc);
        return NULL;
    }

    fseek(arc, *local_dir, SEEK_SET);
    struct arquivo **dir = malloc(sizeof(struct arquivo*) * (*quant_arq));
    struct arquivo *aux;
    char *nomeAux;

    for (int i = 0; i < (*quant_arq); i++){
        //inicia arquivo que sera incerido
        aux = malloc(sizeof(struct arquivo));

        //le tamanho e caminho do arquivo fazendo alocaçao dinamica
        fread(&aux->tamanho_caminho, sizeof(aux->tamanho_caminho), 1, arc);
        nomeAux = malloc(aux->tamanho_caminho * sizeof(char));
        for (int j = 0; j < aux->tamanho_caminho; j++)
            fread(&nomeAux[j], sizeof(char), 1, arc);
        aux->caminho = nomeAux;

        //le o resto
        fread(&aux->pos, sizeof(aux->pos), 1, arc);
        fread(&aux->local, sizeof(aux->local), 1, arc);
        fread(&aux->userID, sizeof(aux->userID), 1, arc);
        fread(&aux->permissoes, sizeof(aux->permissoes), 1, arc);
        fread(&aux->tamanho, sizeof(aux->tamanho), 1, arc);
        fread(&aux->data, sizeof(aux->data), 1, arc);

        dir[i] = aux;
    }
    fclose(arc);
    return dir;
}

int maior_pos(struct arquivo **dir, int quant_arq){
    int maior = 0;
    for (int i = 0; i < quant_arq; i++){
        if (dir[i]->pos > maior)
            maior = dir[i]->pos;
    }

    return maior;
}

struct arquivo **adiciona_diretorio(struct arquivo **dir, int quant_arq, int pos, char *nomeArquivo){
    //obtem info do arquivo
    struct arquivo *arq = obtem_info(nomeArquivo);
    quant_arq++;

    if (quant_arq == 1)
        dir = malloc(sizeof(struct arquivo*));
    else
        dir = realloc(dir, sizeof(struct arquivo*) * quant_arq);

    if (pos == 0)
        arq->local = INICIO_ESCRITA;
    else
        arq->local = dir[pos - 1]->local + dir[pos - 1]->tamanho;

    arq->pos = maior_pos(dir, pos) + 1;
    dir[pos] = arq;
    return dir;
}

struct arquivo **exclui_diretorio(struct arquivo **dir, int quant_arq, int pos){

    size_t excluido = dir[pos]->tamanho;
    int pos_excluido = dir[pos]->pos;
    char *caminho = dir[pos]->caminho;
    for (int i = pos; i < quant_arq - 1; i++){
        dir[i]->tamanho_caminho = dir[i + 1]->tamanho_caminho;
        dir[i]->caminho = dir[i + 1]->caminho;
        dir[i]->local = dir[i + 1]->local - excluido;
        dir[i]->userID = dir[i + 1]->userID;
        dir[i]->permissoes = dir[i + 1]->userID;
        dir[i]->tamanho = dir[i + 1]->tamanho;
        dir[i]->data = dir[i + 1]->data;
        dir[i]->pos = dir[i + 1]->pos;
    }

    for (int j = 0; j < quant_arq - 1; j++){
        if (dir[j]->pos >= pos_excluido)
            dir[j]->pos -= 1;
    }
    free(dir[quant_arq - 1]);
    free(caminho);
    quant_arq--;
    dir = realloc(dir, sizeof(struct arquivo) * quant_arq);
    return dir;
}

void movimenta_arq_dir(struct arquivo **dir, int pos_ant, int pos_arq, int quant_arq){
    int pos_antiga;
    
    if (dir[pos_ant]->pos < dir[pos_arq]->pos){
        pos_antiga = dir[pos_arq]->pos;

        dir[pos_arq]->pos = dir[pos_ant]->pos + 1;
        for (int i = 0; i < quant_arq; i++){
            if ((dir[i]->pos >= dir[pos_arq]->pos) && (dir[i]->pos <= pos_antiga) && (dir[i] != dir[pos_arq]))
                dir[i]->pos = dir[i]->pos + 1;
        }

    }

    else if (dir[pos_ant]->pos > dir[pos_arq]->pos){
        pos_antiga = dir[pos_arq]->pos;

        if (dir[pos_ant]->pos == quant_arq)
            dir[pos_arq]->pos = dir[pos_ant]->pos;
        else 
            dir[pos_arq]->pos = dir[pos_ant]->pos + 1;

        for (int i = 0; i < quant_arq; i++){
            if ((dir[i]->pos >= pos_antiga) && (dir[i]->pos <= dir[pos_arq]->pos) && (dir[i] != dir[pos_arq]))
                dir[i]->pos = dir[i]->pos - 1;
        }
    }
}

int diretorio_existe(char *caminho){
    DIR *dir = opendir(caminho);
    if (dir != NULL){
        closedir(dir);
        return 1;
    }
    return 0;
}

int possui_diretorio(char* nomeArq, int tam_nome){
    int barra = 0;
    for (int i = 0; i < tam_nome; i++){
        if (nomeArq[i] == '/')
            barra++;
    }
    return barra;
}
char* obtem_caminho_relativo(char* caminho, int tam, int *quant_barras){
    char *caminho_relativo;
    if (caminho[0] != '.'){
        if (caminho[0] == '/'){
            caminho_relativo = malloc(sizeof(char) * (tam+1));
            snprintf(caminho_relativo, tam+1, ".%s", caminho);
        }
        else{
            caminho_relativo = malloc(sizeof(char) * (tam+2));
            snprintf(caminho_relativo, tam+2, "./%s", caminho);
            quant_barras++;
        }
    }
    else{
        caminho_relativo = strdup(caminho);
    }
    return caminho_relativo;
}

void cria_hierarquia(char* caminho_relativo, int quant_barras, int tam_caminho){
    int i = 0;
    char *aux;
    int j = 0;
    for (j = 0; (j < tam_caminho) && (i < quant_barras); j++){
        if (caminho_relativo[j] != '/' || i < 1){
            if (j != 0)
                free(aux);
            aux = strndup(caminho_relativo, j + 1);
            if (caminho_relativo[j] == '/')
                i++; 
        }    
        else if (caminho_relativo[j] == '/'){
            if (diretorio_existe(aux)){
                free(aux);
                aux = strndup(caminho_relativo, j + 1);
                i++;
            }
            else{
                int status = mkdir(aux, 0777);
                if (status != 0){
                    printf("Erro ao criar diretorio");
                    free(aux);
                    return;
                }
                free(aux);
                aux = strndup(caminho_relativo, j + 1);
                i++;
            }
        }

    }
    free(aux);
}

void printa_permissao(mode_t perms){
    printf( (perms & S_IRUSR) ? "r" : "-");
    printf( (perms & S_IWUSR) ? "w" : "-");
    printf( (perms & S_IXUSR) ? "x" : "-");
    printf( (perms & S_IRGRP) ? "r" : "-");
    printf( (perms & S_IWGRP) ? "w" : "-");
    printf( (perms & S_IXGRP) ? "x" : "-");
    printf( (perms & S_IROTH) ? "r" : "-");
    printf( (perms & S_IWOTH) ? "w" : "-");
    printf( (perms & S_IXOTH) ? "x" : "-");
}

int conta_digitos(size_t tamanho){
    int conta_dig = 0;
    do{
        conta_dig++;
        tamanho = tamanho / 10;
    }
    while (tamanho != 0);
    return conta_dig;
}

void printaInfo(struct arquivo *arq, int maior_user, int maior_tam) {
    printa_permissao(arq->permissoes);

    struct passwd *pw = getpwuid(arq->userID);
    int espaco_user = maior_user - strlen(pw->pw_name);
    for (int i = 0; i < espaco_user; i++)
        printf(" ");
    printf(" %s", pw->pw_name);

    int espaco_tam = maior_tam - conta_digitos(arq->tamanho);
    for (int j = 0; j < espaco_tam; j++)
        printf(" ");
    printf(" %ld", arq->tamanho);

    struct tm *time = localtime(&arq->data);
    printf(" %4d-%02d-%02d", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday);
    printf(" %02d:%02d", time->tm_hour, time->tm_min);

    printf(" %s", arq->caminho);
}

//printa o deiretorio de forma ordenada em relação a pos de cada arquivo
void printaDiretorio(struct arquivo **dir, int quant_arq, int maior_user, int maior_tam){

    int m = 1;

    for (int i = 0; i < quant_arq; i++) {
        for (int j = 0; j < quant_arq; j++){
            if (dir[j]->pos == m)
                printaInfo(dir[j], maior_user, maior_tam);
        }
        printf("\n");
        m++;
    }
}


void insere_arquivo(char* nomeArchiver, char* nomeArq, int a){

    if (access(nomeArchiver, F_OK) != 0){
        inicia_archiver(nomeArchiver);
    }

    //obtem informaçoes sobre o dicionario
    int quant_arq;
    size_t local_dir;
    struct arquivo **dir = obtem_diretorio(nomeArchiver, &local_dir, &quant_arq);

    //Substituir ou excluir o já existente
    int i = 0;
    while (i < quant_arq) {
        if (strcmp(dir[i]->caminho, nomeArq) == 0) {
            struct arquivo *novo = obtem_info(nomeArq);
            time_t data_antigo = dir[i]->data;
            if (!a || novo->data > data_antigo){
                free(novo->caminho);
                free(novo);
                apaga_arquivo(nomeArchiver, nomeArq);
            }
            else {
                free(novo->caminho);
                free(novo);
                libera_diretorio(dir, quant_arq);
                return;
            }
            libera_diretorio(dir, quant_arq);
            dir = obtem_diretorio(nomeArchiver, &local_dir, &quant_arq);
            if (quant_arq == 0)
                break;
        }
        i++;
    }
    //abre o arquivo archive e novo arquivo
    FILE *archive = fopen(nomeArchiver, "r+");
    FILE *arq = fopen(nomeArq, "r");
    if (!archive || !arq){
        if (!arq && archive)
            fclose(archive);
        if (arq && !archive)
            fclose(arq);
        libera_diretorio(dir, quant_arq);
        perror("Erro ao abir arquivos\n");
        exit(1);
    }

    //adiciona no fim do diretorio
    dir = adiciona_diretorio(dir, quant_arq, quant_arq, nomeArq);
    
    quant_arq++;
    size_t novo_local_dir = dir[quant_arq - 1]->local + dir[quant_arq - 1]->tamanho;
    rewind(archive);
    fwrite(&quant_arq, sizeof(int), 1, archive);
    fwrite(&novo_local_dir, sizeof(size_t), 1, archive);

    fseek(archive, local_dir, SEEK_SET);
    transporta(arq, archive, dir[quant_arq - 1]->tamanho);
    escreve_diretorio(archive, dir, quant_arq);

    fclose(archive);
    fclose(arq);
    libera_diretorio(dir, quant_arq);

}

void apaga_arquivo(char *nomeArchive, char *nomeArq){

    if (access(nomeArchive, F_OK) != 0){
        inicia_archiver(nomeArchive);
    }

    int quant_arq;
    size_t local_dir;
    struct arquivo **dir = obtem_diretorio(nomeArchive, &local_dir, &quant_arq);

    int pos = 0;
    while (pos < quant_arq){
        if (strcmp(dir[pos]->caminho, nomeArq) == 0){  
            break;
        }
        pos++;
    }

    if (pos == quant_arq){
        printf("Arquivo não encontrado\n");
        libera_diretorio(dir, quant_arq);
        return;
    }

    size_t tam_arq = dir[pos]->tamanho;
    size_t local_arq = dir[pos]->local;
    size_t local_prox = dir[pos]->local + dir[pos]->tamanho;

    dir = exclui_diretorio(dir, quant_arq, pos);

    quant_arq--;
    size_t novo_local_dir = local_dir - tam_arq;
    
    FILE *archive = fopen(nomeArchive, "r+");
    if (!archive){
        libera_diretorio(dir, quant_arq);
        perror("Erro ao abir Archive\n");
        exit(1);
    }

    rewind(archive);
    fwrite(&quant_arq, sizeof(int), 1, archive);
    fwrite(&novo_local_dir, sizeof(size_t), 1, archive);

    size_t tam_copiado = local_dir - local_prox;
    copia_parte(archive, tam_copiado, local_arq, local_prox);
    fseek(archive, novo_local_dir, SEEK_SET);
    escreve_diretorio(archive, dir, quant_arq);

    size_t pos_truncate = ftell(archive);

    fclose(archive);
    truncate(nomeArchive, pos_truncate);
    libera_diretorio(dir, quant_arq);
}

void move_arquivo(char* nomeArchive, char* nomeAnterior, char* nomeArq){
    
    if (access(nomeArchive, F_OK) != 0){
        printf("ERRO: O Archiver não existe");
        exit(1);
    }

    int quant_arq;
    size_t local_dir;
    struct arquivo **dir = obtem_diretorio(nomeArchive, &local_dir, &quant_arq);

    //Obtem a posição do arquivo de posição de base
    int pos_ant;
    for(pos_ant = 0; pos_ant < quant_arq; pos_ant++){
        if (strcmp(dir[pos_ant]->caminho, nomeAnterior) == 0)
            break;
    }
    if (pos_ant == quant_arq){
        printf("O arquivo de posição base não existe");
        libera_diretorio(dir, quant_arq);
        exit(1);
    }

    //Obtem a posição do arquivo que deve ser movimentado
    int pos_arq;
    for(pos_arq = 0; pos_arq < quant_arq; pos_arq++){
        if (strcmp(dir[pos_arq]->caminho, nomeArq) == 0)
            break;
    }
    if (pos_arq == quant_arq){
        printf("O arquivo que deve ser movimentado não existe");
        libera_diretorio(dir, quant_arq);
        exit(1);
    }

    //movimenta no diretorio o arquivo
    movimenta_arq_dir(dir, pos_ant, pos_arq, quant_arq);
    FILE *archive = fopen(nomeArchive, "r+");
    if (!archive){
        libera_diretorio(dir, quant_arq);
        perror("Erro ao abrir Archive");
        exit(1);
    }


    fseek(archive, local_dir, SEEK_CUR);
    escreve_diretorio(archive, dir, quant_arq);
    fclose(archive);
    libera_diretorio(dir, quant_arq);
}

void lista_arquivos(char *nomeArchive){

    if (access(nomeArchive, F_OK) != 0){
        printf("ERRO: O Archiver não existe");
        exit(1);
    }

    int quant_arq; size_t local_dir;
    struct arquivo** dir = obtem_diretorio(nomeArchive, &local_dir, &quant_arq);
    struct passwd *pw;

    int maior_user = 0;
    int maior_tam = 0;
    for (int i = 0; i < quant_arq; i++){
        pw = getpwuid(dir[i]->userID);

        if (strlen(pw->pw_name) > maior_user)
            maior_user = strlen(pw->pw_name);

        if (conta_digitos(dir[i]->tamanho) > maior_tam)
            maior_tam = conta_digitos(dir[i]->tamanho);
    }

    printaDiretorio(dir, quant_arq, maior_user, maior_tam);
    libera_diretorio(dir, quant_arq);
}


void extrai_arquivo(char* nomeArchive, char* nomeArq){

    if (access(nomeArchive, F_OK) != 0){
        printf("ERRO: O Archiver não existe");
        exit(1);
    }

    int quant_arq; size_t local_dir;
    struct arquivo** dir = obtem_diretorio(nomeArchive, &local_dir, &quant_arq);

    //obtem posição no vetor do arquivo
    int pos = 0;
    while (pos < quant_arq){
        if (strcmp(dir[pos]->caminho, nomeArq) == 0){  
            break;
        }
        pos++;
    }
    if (pos == quant_arq){
        printf("Arquivo não encontrado\n");
        libera_diretorio(dir, quant_arq);
        return;
    }

    //obtem string apenas com parte do diretorio
    char* caminho = dir[pos]->caminho;
    char* caminho_relativo;

    int quant_barras = possui_diretorio(caminho, dir[pos]->tamanho_caminho);
    if (quant_barras){

        caminho_relativo = obtem_caminho_relativo(caminho, dir[pos]->tamanho_caminho, &quant_barras);
        cria_hierarquia(caminho_relativo, quant_barras, strlen(caminho_relativo));

        FILE *arq = fopen(caminho_relativo, "w+b");
        FILE *archive = fopen(nomeArchive, "r");
        if (!arq || !archive){
            if (!arq && archive)
                fclose(archive);
            if (arq && !archive)
                fclose(arq);
            libera_diretorio(dir, quant_arq);
            perror("Erro ao abrir arquivo");
            exit(1);
        }
        fseek(archive, dir[pos]->local, SEEK_SET);
        transporta(archive, arq, dir[pos]->tamanho);
        fclose(arq);
        fclose(archive);

    }
    else{
        FILE *arq = fopen(dir[pos]->caminho, "w+b");
        FILE *archive = fopen(nomeArchive, "r");
        if (!arq || !archive){
            if (!arq && archive)
                fclose(archive);
            if (arq && !archive)
                fclose(arq);
            libera_diretorio(dir, quant_arq);
            perror("Erro ao abrir arquivo");
            exit(1);
        }
        fseek(archive, dir[pos]->local, SEEK_SET);
        transporta(archive, arq, dir[pos]->tamanho);
        fclose(arq);
        fclose(archive);

    }
    free(caminho_relativo);
    libera_diretorio(dir, quant_arq);
}

void extrai_todos(char* nomeArchive){
   
    if (access(nomeArchive, F_OK) != 0){
        printf("ERRO: O Archiver não existe");
        exit(1);
    }

    int quant_arq; size_t local_dir;
    struct arquivo** dir = obtem_diretorio(nomeArchive, &local_dir, &quant_arq);

    for (int i = 0; i < quant_arq; i++){
        char* caminho = dir[i]->caminho;
        char* caminho_relativo;

        int quant_barras = possui_diretorio(caminho, dir[i]->tamanho_caminho);
        if (quant_barras){

            caminho_relativo = obtem_caminho_relativo(caminho, dir[i]->tamanho_caminho, &quant_barras);
            cria_hierarquia(caminho_relativo, quant_barras, strlen(caminho_relativo));

            FILE *arq = fopen(caminho_relativo, "w+b");
            FILE *archive = fopen(nomeArchive, "r");
            if (!arq || !archive){
                if (!arq && archive)
                    fclose(archive);
                if (arq && !archive)
                    fclose(arq);
                libera_diretorio(dir, quant_arq);
                perror("Erro ao abrir arquivo");
                exit(1);
            }
            fseek(archive, dir[i]->local, SEEK_SET);
            transporta(archive, arq, dir[i]->tamanho);
            fclose(arq);
            fclose(archive);

        }
        else{
            FILE *arq = fopen(dir[i]->caminho, "w+b");
            FILE *archive = fopen(nomeArchive, "r");
            if (!arq || !archive){
                if (!arq && archive)
                    fclose(archive);
                if (arq && !archive)
                    fclose(arq);
                libera_diretorio(dir, quant_arq);
                perror("Erro ao abrir arquivo");
                exit(1);
            }
            fseek(archive, dir[i]->local, SEEK_SET);
            transporta(archive, arq, dir[i]->tamanho);
            fclose(arq);
            fclose(archive);

        }
        free(caminho_relativo);
    }
    libera_diretorio(dir, quant_arq);
}

void ajuda(){
    printf("Uso correto: vina++ <op> <archive> [membro(s)]\n");
    printf("op = -i para incerir arquivo, caso o membro ja exista é substituido\n");
    printf("op = -a mesmo comportamento de -i mas a substituição acontece apenas se o novo membro for mais recente\n");
    printf("op = -m [target] o arquivo é movido para a posição apos o target\n");
    printf("op = -x extrai os arquivos indicados, caso nenhum arquivo seja indicado todos são extraidos\n");
    printf("op = -r remove os membros indicados\n");
    printf("op = -c lista o conteudo dos arquivos pertencentes ao archive\n");
    printf("op = -h imprime a mensagem de ajuda\n");
}
