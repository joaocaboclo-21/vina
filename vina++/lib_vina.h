#ifndef LIB_VINA_H
#define LIB_VINA_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <utime.h>
#include <pwd.h>
#include <dirent.h>

struct arquivo{
    int tamanho_caminho;
    char *caminho;
    int pos;
    off_t local; 
    uid_t userID;
    mode_t permissoes;
    size_t tamanho;
    time_t data;
};

void insere_arquivo(char* nomeArchiver, char* nomeArq, int a);

void apaga_arquivo(char *nomeArchive, char *nomeArq);

void move_arquivo(char* nomeArchive, char* nomeAnterior, char* nomeArq);

void lista_arquivos(char *nomeArchive);

void extrai_arquivo(char* nomeArchive, char* nomeArq);

void extrai_todos(char* nomeArchive);

void ajuda();


#endif