
#include <stdio.h>
#include <stdlib.h>
#include "lib_vina.h"

int main(int argc, char *argv[]){
    
    char *opcao = argv[1];
    char *nomeArchive, *nomeArq, *nomeAnterior;

    if ( argc <= 2){

        if (strcmp(opcao, "-h") == 0){
            ajuda();
        }
        else{
            printf("ERRO: Uso correto: vina++ <op> <archive> [membro(s)]\n");
            exit(1);
        }
    }

    

    if (strcmp(opcao, "-i") == 0){
        nomeArchive = argv[2];
        for (int i = 3; i < argc; i++){
            nomeArq = argv[i];
            insere_arquivo(nomeArchive, nomeArq, 0);
        }
        return 0;
    }

    if (strcmp(opcao, "-a") == 0){
        nomeArchive = argv[2];
        for (int i = 3; i < argc; i++){
            nomeArq = argv[i];
            insere_arquivo(nomeArchive, nomeArq, 1);
        }
        return 0;
    }

    if (strcmp(opcao, "-r") == 0){
        nomeArchive = argv[2];
        for (int i = 3; i < argc; i++){
            nomeArq = argv[i];
            apaga_arquivo(nomeArchive, nomeArq);
        }
        return 0;
    }

    if (strcmp(opcao, "-m") == 0){
        nomeAnterior = argv[2];
        nomeArchive = argv[3];
        nomeArq = argv[4];
        move_arquivo(nomeArchive, nomeAnterior, nomeArq);
        return 0;
    }

    if (strcmp(opcao, "-c") == 0){
        nomeArchive = argv[2];
        lista_arquivos(nomeArchive);
        return 0;
    }

    if (strcmp(opcao, "-x") == 0){
        nomeArchive = argv[2];
        if (argc > 3){
            for (int i = 3; i < argc; i++){
                nomeArq = argv[i];
                extrai_arquivo(nomeArchive, nomeArq);
            }
        }
        else{
            extrai_todos(nomeArchive);
        }
        return 0;
    }

    if (strcmp(opcao, "-h") == 0){
        ajuda();
    }

    else{       
        printf("ERRO: Uso correto: vina++ <op> <archive> [membro(s)]\n");
        exit(1);
    }

}
